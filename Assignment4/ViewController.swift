//
//  ViewController.swift
//  Assignment4
//
//  Created by Светлов Андрей on 29.08.2018.
//  Copyright © 2018 svetloff. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let machine = CoffeMachine.init(modelOfCoffeMachine: .T100)
    
    @IBOutlet weak var resultLabel: UILabel!
    
    @IBOutlet weak var modelLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        resultLabel.text = "Good day!!!"
        modelLabel.text = machine.description
    }

    @IBAction func drinkButtonPressed(_ sender: UIButton) {
        let selectedDrink  = sender.tag
        var result:(Bool, String)
        
        switch selectedDrink {
        case 1:
            result = machine.getEspresso()
        case 2:
            result = machine.getAmericano()
        case 3:
            result = machine.getLate()
        case 4:
            result = machine.getAmericanoWithMilk()
        default:
            result = (true, "Drink not found")
        }
        
        processResult(result)
    }
    
    
    @IBAction func serviceButtonPressed(_ sender: UIButton) {
        let selectedService  = sender.tag
        var result:(Bool, String)
        
        switch selectedService {
        case 1:
            result = machine.clearKnockBox()
        case 2:
            result = machine.addCoffee(amount: 100)
        case 3:
            result = machine.addWater(amount: 500)
        case 4:
            result = machine.addMilk(amount: 100)
        default:
            result = (true, "Service not found")
        }
        
        processResult(result)
    }
    
    func processResult(_ result:(error:Bool, message:String)) {
        resultLabel.text = result.message
        if result.error {
            resultLabel.textColor = UIColor.red
        }
        else {
            resultLabel.textColor = UIColor.black
        }
    }
}

