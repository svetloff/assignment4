//
//  CoffeMachine.swift
//  Assignment4
//
//  Created by Светлов Андрей on 29.08.2018.
//  Copyright © 2018 svetloff. All rights reserved.
//

import UIKit


class CoffeMachine: NSObject {
    
    enum Model: Int {
        case T100 = 1
        case T1000 = 2
    }
    
    enum Drink: Int {
        case espresso = 1
        case americano = 2
        case americanoWithMilk = 3
        case late = 4
    }
    
    private let model: Model
    private var capacityTankOfCofee = 0     //gramms
    private var amountOfCoffee = 0          //gramms
    private var capacityTankOfWater = 0     //milliters
    private var amountOfWater = 0           //milliters
    private var capacityTankOfMilk = 0      //milliters
    private var amountOfMilk = 0            //milliters
    private var capacityOfKnockBox = 0      //gramms
    private var amountInKnockBox = 0        //gramms
    
    override var description: String {
        var result = ""
        result  = "Coffe machine model: \(model)"
        return result
    }
    
    init(modelOfCoffeMachine: Model) {
        model = modelOfCoffeMachine
        switch model {
        case .T100:
            capacityTankOfCofee = 100
            capacityOfKnockBox = 20
            capacityTankOfWater = 500
            capacityTankOfMilk = 100
        case .T1000:
            capacityTankOfCofee = 200
            capacityOfKnockBox = 50
            capacityTankOfWater = 1000
            capacityTankOfMilk = 100
        }
    }
    
    func getEspresso() -> (Bool, String){
        var result = (error: false, messge: "")
        
        result = runMachine(drink: .espresso)
        
        if !result.error {
            result.messge = "Your espresso COMMANDER.\n\t\t\t" + result.messge
        }
        return result
    }
    
    func getAmericano() -> (Bool, String){
        var result = (error: false, messge: "")
        
        result = runMachine(drink: .americano)
        
        if !result.error {
            result.messge = "Your americano COMMANDER.\n\t\t\t" + result.messge
        }
        
        return result
    }
    
    func getAmericanoWithMilk() -> (Bool, String){
        var result = (error: false, messge: "")
        
        result = runMachine(drink: .americanoWithMilk)
        
        if !result.error {
            result.messge = "Your americano COMMANDER.\n\t\t\t" + result.messge
        }
        
        return result
    }
    
    func getLate() -> (Bool, String) {
        var result = (error: false, messge: "")
        
        result = runMachine(drink: .late)
        
        if !result.error {
            result.messge = "Your late COMMANDER.\n\t\t\t" + result.messge
        }
        
        return result
    }
    
    private func runMachine(drink: Drink) -> (Bool, String) {
        var result = (error: false, messge: "")
        let recept = getRecept(drink: drink)
        
        result = getMachineIsRedy(recept: recept)
        
        if !result.error {
            amountOfWater -= recept["water"]!
            amountOfCoffee -= recept["coffee"]!
            amountOfMilk -= recept["milk"]!
            amountInKnockBox += recept["coffee"]!
            
            amountOfWater = amountOfWater < 0 ? 0 : amountOfWater
            amountOfCoffee = amountOfCoffee < 0 ? 0 : amountOfCoffee
            amountOfMilk = amountOfMilk < 0 ? 0 : amountOfMilk
            
            //result.messge = "Good day!!!"
        }
        
        return result
    }
    
    private func getMachineIsRedy(recept:[String: Int]) -> (Bool, String) {
        var result = (error: false, messge: "")
        
        if amountOfWater <= 0 {
            result  = (error: true, messge: "Water tank is epty")
            return result
        }
        
        if amountOfCoffee <= 0 {
            result  = (error: true, messge: "Coffee tank is epty")
            return result
        }
        
        if amountOfMilk <= 0 && recept["milk"]! > 0 {
            result  = (error: true, messge: "Milk tank is epty")
            return result
        }
        
        if amountInKnockBox >= capacityOfKnockBox {
            result  = (error: true, messge: "Knock box is full")
            return result
        }
        
        return result
    }
    
    private func getRecept(drink: Drink) -> [String: Int] {
        var result = ["coffee": 0, "water": 0, "milk": 0]
        
        switch drink {
        case .espresso:
            result = ["coffee": 10, "water": 30, "milk": 0]
        case .americano:
            result = ["coffee": 10, "water": 60, "milk": 0]
        case .americanoWithMilk:
            result = ["coffee": 10, "water": 60, "milk": 20]
        case .late:
            result = ["coffee": 10, "water": 30, "milk": 100]
        }
        
        return result
    }
    
    func addCoffee(amount: Int) -> (Bool, String) {
        var result = (error: false, messge: "")
        
        amountOfCoffee = addToTank(amount: amount, capacityTank: capacityTankOfCofee, amountInTank: amountOfCoffee)
        
        result.messge = "Coffee in tank now = \(amountOfCoffee)"
        
        return result
    }
    
    func addWater(amount: Int) -> (Bool, String) {
        var result = (error: false, messge: "")
        
        amountOfWater = addToTank(amount: amount, capacityTank: capacityTankOfWater, amountInTank: amountOfWater)
        
        result.messge = "Water in tank now = \(amountOfWater)"
        
        return result
    }
    
    func addMilk(amount: Int) -> (Bool, String) {
        var result = (error: false, messge: "")
        
        amountOfMilk = addToTank(amount: amount, capacityTank: capacityTankOfMilk, amountInTank: amountOfMilk)
        
        result.messge = "Milk in tank now = \(amountOfMilk)"
        
        return result
    }
    
    func clearKnockBox() -> (Bool, String) {
        var result = (error: false, messge: "")
        
        amountInKnockBox = 0
        
        result.messge = "Knock box is empty"
        
        return result
    }
    
    private func addToTank(amount: Int, capacityTank: Int, amountInTank: Int) -> Int{
        var result = 0
        
        if  amount > (capacityTank - amountInTank) {
            result = capacityTank
        }
        else {
            result += amount
        }
        
        return result
    }

}
